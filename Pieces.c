/*
 * Project: TetriC
 */
#include "Pieces.h"

/*** INIT ***/
void initPiece(Piece *piece, int type, int x, int y)
{
    piece->point.x = x;
    piece->point.y = y;
    piece->type = type;
    piece->height = 0;
    piece->orientation = 0;

    switch(type)
    {
        case P_O:
            piece->color = C_BLUE;
            piece->height = 2;
            piece->orientation = 0;
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_O_0[x][y];
            break;
        case P_L:
            piece->color = C_RED;
            piece->height = 3;
            piece->orientation = 0;
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_L_0[x][y];
            break;
        case P_T:
            piece->color = C_YELLOW;
            piece->height = 2;
            piece->orientation = 0;
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_T_0[x][y];
            break;
        case P_Z:
            piece->color = C_MAGENTA;
            piece->height = 2;
            piece->orientation = 0;
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_Z_0[x][y];
            break;
        case P_I:
            piece->color = C_GREEN;
            piece->height = 4;
            piece->orientation = 0;
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_I_0[x][y];
            break;
        default:
            printf("Invalid or not yet implemented piece");
    }
}

/*** MOUVEMENT ***/
void goDownP(Piece *piece)   { piece->point.y += 1; }
void goLeftP(Piece *piece)   { piece->point.x -= 1; }
void goRightP(Piece *piece)  { piece->point.x += 1; }

/*** PIECES ***/
static void _updateO(Piece *piece, int position)
{
    piece->orientation = (piece->orientation+1) %8;
    int x, y = 0;
    for(x = 0; x<4; x++)
        for(y=0; y<4; y++)
            piece->pattern[x][y] = PIECE_O_0[x][y];
}

static void _updateL(Piece *piece, int position)
{
    //INIT
    position = position %8;
    piece->orientation = (piece->orientation+1) %8;
    int x, y = 0;
    switch(position)
    {
        case 0:
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_L_0[x][y];
            break;
        case 1:
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_L_1[x][y];
            break;
        case 2:
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_L_2[x][y];
            break;
        case 3:
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_L_3[x][y];
            break;
        case 4:
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_L_4[x][y];
            break;
        case 5:
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_L_5[x][y];
            break;
        case 6:
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_L_6[x][y];
            break;
        case 7:
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_L_7[x][y];
            break;
        default:
            printf("rotate() invalid position: %d\n", piece->orientation);
            break;
    }
}

static void _updateT(Piece *piece, int position)
{
    //INIT
    position = position %8;
    piece->orientation = (piece->orientation+1) %8;
    int x, y = 0;
    switch(position)
    {
        case 0:
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_T_0[x][y];
            break;
        case 1:
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_T_1[x][y];
            break;
        case 2:
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_T_2[x][y];
            break;
        case 3:
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_T_3[x][y];
            break;
        case 4:
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_T_4[x][y];
            break;
        case 5:
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_T_5[x][y];
            break;
        case 6:
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_T_6[x][y];
            break;
        case 7:
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_T_7[x][y];
            break;
        default:
            printf("rotate() invalid position: %d\n", piece->orientation);
            break;
    }
}

static void _updateZ(Piece *piece, int position)
{
    //INIT
    position = position %8;
    piece->orientation = (piece->orientation+1) %8;
    int x, y = 0;
    switch(position)
    {
        case 0:
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_Z_0[x][y];
            break;
        case 1:
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_Z_1[x][y];
            break;
        case 2:
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_Z_2[x][y];
            break;
        case 3:
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_Z_3[x][y];
            break;
        case 4:
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_Z_4[x][y];
            break;
        case 5:
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_Z_5[x][y];
            break;
        case 6:
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_Z_6[x][y];
            break;
        case 7:
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_Z_7[x][y];
            break;
        default:
            printf("rotate() invalid position: %d\n", piece->orientation);
            break;
    }
}

static void _updateI(Piece *piece, int position)
{
//INIT
    position = position %8;
    piece->orientation = (piece->orientation+1) %8;
    int x, y = 0;
    switch( (position) % 2)
    {
        case 0:
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_I_0[x][y];
            break;
        case 1:
            for(x = 0; x<4; x++)
                for(y=0; y<4; y++)
                    piece->pattern[x][y] = PIECE_I_1[x][y];
            break;
        default:
            printf("rotate() invalid position: %d\n", piece->orientation);
            break;
    }
}

void updatePiece(Piece *piece, int position)
/* Call this method to init or update the orientation of a piece */
{

    switch(piece->type)
    {
        case P_O:
            _updateO(piece, position);
            break;
        case P_L:
            _updateL(piece, position);
            break;
        case P_T:
            _updateT(piece, position);
            break;
        case P_Z:
            _updateZ(piece, position);
            break;
        case P_I:
            _updateI(piece, position);
            break;
        default:
            printf("rotate() invalid piece type: %d\n", piece->type);
            break;
    }
}

Piece copyPiece(Piece *piece)
{
    int x, y = 0;
    Piece copy_piece;
    copy_piece.color = piece->color;
    copy_piece.height = piece->height;
    copy_piece.orientation = piece->orientation;
    copy_piece.point.x = piece->point.x;
    copy_piece.point.y = piece->point.y;
    copy_piece.type = piece->type;

    for(x=0; x < 4; x++)
        for(y=0; y < 4; y++)
            copy_piece.pattern[x][y] = piece->pattern[x][y];

    return copy_piece;
}


/******************************************************************/
/**************************DEBUG***********************************/
/******************************************************************/
/**/
/**/    void dbgDrawPiece(Piece p)
/**/    {
/**/        int x, y = 0;
/**/        for(y = 0; y<4; y++)
/**/        {
/**/            for(x=0; x<4; x++)
/**/            {
/**/                if(p.pattern[x][y] != P_EMPT)
/**/                    color(p.color);
/**/                printf("%c", p.pattern[x][y]);
/**/                color(C_DEFAULT);
/**/            }
/**/            printf("\n");
/**/        }
/**/    }
/**/
/******************************************************************/
/******************************************************************/
