/*
 * Project: TetriC
 */
 #include "Grille.h"

/*** DRAW ***/
void afficheGrille(Grid g)
{
    int x=0;
    int y=0;
    for(y=0; y<25; y++)
    {
        printf("|");
        for(x=0; x<12; x++)
        {
            if(g.grid[x][y] == 'O')
                color(C_BLUE);
            else if(g.grid[x][y] == 'L')
                color(C_RED);
            else if(g.grid[x][y] == 'T')
                color(C_YELLOW);
            else if(g.grid[x][y] == 'Z')
                color(C_MAGENTA);
            else if(g.grid[x][y] == 'I')
                color(C_GREEN);
            else color(C_DEFAULT);
            printf("%c", g.grid[x][y]);
            color(C_DEFAULT);
        }
        //printf("|\n");
        printf("%d\n", y);
    }
}

void afficheBloc(char bloc[4][4])
{
    int x=0;
    int y=0;
    for(y=0; y<4; y++)
    {
        for(x=0; x<4; x++)
            printf("%c", bloc[x][y]);
        printf("\n");
    }
}

/*** FILL ***/
void initGrid(Grid *g, char carac) //Remplit la grille par le caractère carac
{
    int x, y=0;
    g->score = 0;
    g->height = 0;
    g->totalLines = 0;

    for(y=0; y<25; y++)
    {
        for(x=0; x<12; x++)
        {
            g->grid[x][y]=carac;
        }
    }
}

void recopieGrille(char grille[12][25], char copie[12][25])
{
    int x=0;
    int y=0;
    for(y=0; y<25; y++)
    {
        for(x=0; x<12; x++)
        {
            copie[x][y] = grille[x][y];
        }
    }
}

int canGoDown(Grid g, Piece p)
{
    int x=0;
    int y=0;
    for(y=0; y<4; y++)
    {
        for(x=0; x<4; x++)
        {
            if(p.pattern[x][y]!= P_EMPT)
            {
                if(p.point.y+y+1 > 24 || g.grid[p.point.x+x][p.point.y+y+1] != G_EMPT)
                 {
                    //printf("DBG - canGoDown() - p->%d/%d is first NOK\n",p.point.x+x,p.point.y);
                    return 0;
                }
            }
        }
    }
    return 1;
}

void putInGrid(Grid *g, Piece p)
{
    int x=0;
    int y=0;
    int coord_x = p.point.x;
    int coord_y = p.point.y;
    for(y=0; y<4; y++)
    {
        for(x=0; x<4; x++)
        {
            if(p.pattern[x][y]!=P_EMPT)
                g->grid[coord_x+x][coord_y+y]=p.pattern[x][y];
        }
    }
}

int canGoRight(Grid g, Piece p)
{
    int coord_x = p.point.x;
    int coord_y = p.point.y;
    int x=0;
    int y=0;
    for(y=0; y<4; y++)
    {
        for(x=0; x<4; x++)
        {
            if(p.pattern[x][y]!=P_EMPT)
            {
                if(g.grid[coord_x+x+1][coord_y+y]!=G_EMPT || coord_x+x+1 >= 12)
                {
                    return 0;
                }
            }
        }
    }
    return 1;
}

int canGoLeft(char grille[12][25], Piece p)
{
    int coord_x = p.point.x;
    int coord_y = p.point.y;
    int x=0;
    int y=0;
    for(y=0; y<4; y++)
    {
        for(x=0; x<4; x++)
        {
            if(p.pattern[x][y]!=P_EMPT)
            {
                if(grille[coord_x+x-1][coord_y+y]!=G_EMPT || coord_x+x-1 < 0)
                {
                    return 0;
                }
            }
        }
    }
    return 1;
}

int heightGrid(Grid g)
{
    int x=0;
    int y=0;
    for(y=0; y<25; y++)
    {
        for(x=0; x<12; x++)
        {
            if(g.grid[x][y] != G_EMPT)
            {
                return 25-y;
            }
        }
    }
    return 0;
}

int fullLines(Grid *g)
{
    int x=0;
    int y=0;
    int fullLines = 0;
    for(y=0; y<25; y++)
    {
        int isFull = 1;
        for(x=0; x<12; x++)
        {
            if(g->grid[x][y] == G_EMPT)
                isFull=0;
        }
        if(isFull==1)
            fullLines++;
    }
    switch(fullLines)
    {
        case 1:
            g->score += 40;
            break;
        case 2:
            g->score += 100;
            break;
        case 3:
            g->score += 300;
            break;
        case 4:
            g->score += 1200;
            break;
        default:
            g->score += 0;
            break;
    }
    g->totalLines += fullLines;
    return fullLines;
}

void removeFullLines(Grid *g)
{
    int x=0;
    int y=0;
    for(y=0; y<25; y++)
    {
        int isFull = 1;
        for(x=0; x<12; x++)
        {
            if(g->grid[x][y] == G_EMPT)
                isFull=0;
        }
        if(isFull==1)
            shiftGrid(g->grid, y);
    }
}

void handleFullLines(Grid *g) //TODO Merge fullines() & removefulllines() cause of same loops struct
{
    int x, y = 0;
}

void shiftGrid(char grille[12][25], int y)
{
    int x=0;
    int coord_y;
    for(coord_y=y; coord_y>=1; coord_y--)
    {
        for(x=0; x<12; x++)
        {
            grille[x][coord_y]=grille[x][coord_y-1];
        }
    }
}

int LineToPoints(int numberOfLines)
{
    switch(numberOfLines)
    {
        case 1:
            return ONE_FULL;
            break;
        case 2:
            return TWO_FULL;
            break;
        case 3:
            return THREE_FULL;
            break;
        case 4:
            return FOUR_FULL;
            break;
        default:
            return 0;
            break;
    }
}

void dbgCreateLine(char grille[12][25], int y)
{
    int x=0;
    for(x=0; x<12; x++)
        grille[x][y]=P_FULL;
}

Point getLowest(Grid g, Piece p)
{
    while(canGoDown(g, p))
        p.point.y++;
    return p.point;
}

int scanContactPoints(Grid *g, Point point)
{
    int x = point.x;
    int y = point.y;
    int contacts = 0;

    for(y=point.y-1; y<point.y+1; y++)
        for(x=point.x-1; x<point.x+1; x++)
            if(( x > 1 && x < 11) || (y > 1 && y < 25))
                if(g->grid[x][y] != P_EMPT || g->grid[x][y] != G_EMPT)
                    contacts++;
    return contacts;
}

int scanContactPointsWithPiece(Grid g, Piece piece, Point p)
{
    int x=0;
    int y=0;
    int contacts=0;
    for(y=0; y<4; y++)
    {
        for(x=0; x<4; x++)
        {
            if(piece.pattern[x][y]!=P_EMPT)
            {
                if(p.x+x-1 > 0)//Si on est pas en bord de grille a gauche
                {
                    if(g.grid[p.x+x-1][p.y+y]!=G_EMPT)//Si le point balayé à quelqu'un a gauche
                        contacts++;
                }
                if(p.x+x+1 < 12)//Si on est pas en bord de grille a droite
                {
                    if(g.grid[p.x+x+1][p.y+y]!=G_EMPT)//Si le point balayé à quelqu'un a droite
                        contacts++;
                }
                if(p.y+y+1 < 24)//Si on est pas en bout de grille en bas
                {
                    if(g.grid[p.x+x][p.y+y+1]!=G_EMPT)//Si le point balayé à quelqu'un en dessous
                        contacts++;
                }
                //TODO Piece au dessus si AI vraiment intelligente
            }
        }
    }
    return contacts;
}

int scanContactPointsWithGridY(Grid g, Piece piece, Point p)
{
    int x=0;
    int y=0;
    int contacts=0;
    for(y=0; y<4; y++)
    {
        for(x=0; x<4; x++)
        {
            if(piece.pattern[x][y]!=P_EMPT)
            {
                if(p.y+y+1 > 24)//Si on est en bout de grille en bas
                {
                        contacts++;
                }
                //TODO Piece au dessus si AI vraiment intelligente
            }
        }
    }
    return contacts;
}

int scanContactPointsWithGridX(Grid g, Piece piece, Point p)
{
    int x=0;
    int y=0;
    int contacts=0;
    for(y=0; y<4; y++)
    {
        for(x=0; x<4; x++)
        {
            if(piece.pattern[x][y]!=P_EMPT)
            {
                if(p.x+x-1 <=0)//Si on est en bord de grille
                {
                        contacts++;
                }
                if(p.x+x+1 >= 11)//Si on est en bord de grille
                {
                        contacts++;
                }
                //TODO Piece au dessus si AI vraiment intelligente
            }
        }
    }
    return contacts;
}

int scanHoleMade(Grid g, Piece piece, Point p)
{
    int x=0;
    int y=0;
    int contacts=0;
    for(y=4; y>=0; y--)
    {
        for(x=0; x<4; x++)
        {
            if(piece.pattern[x][y]!=P_EMPT)
            {
                if(p.y+y+1 < 24)//Si on est en bout de grille en bas
                {
                     if(g.grid[p.x+x][p.y+y+1]==G_EMPT)//Si le point balayé à quelqu'un en dessous
                        if(y<4 && piece.pattern[x][y+1]==P_EMPT)
                            contacts++;
                }
            }
        }
    }
    return contacts;
}

int scanDiffHeight(Grid g, Piece piece, Point p)
{
    int height = heightGrid(g);
    piece.point = p;
    putInGrid(&g, piece);
    int new_height = heightGrid(g);
    if(new_height-height==0)
        return 0;
    else
        return new_height - height;
}

int scanLineFulFilled(Grid g, Piece piece, Point p)
{
    int line = 0;
    piece.point = p;
    putInGrid(&g, piece);
    line = fullLines(&g);
    return line;

}

int scanMinHeight(Grid g)
{
    int x, y = 0;
    int height_min = 0;
    int height_min_old = 26;

    for(x= 0; x < GAME_WIDTH; x++)
    {
        while( (g.grid[x][y+1] == G_EMPT) && y < GAME_HEIGHT-1 )
        {
            height_min++;
            y++;
        }
        height_min = 25-height_min;
        if(height_min < height_min_old)
            height_min_old = height_min;
        height_min = 0;
    }
    return height_min_old;
}

int scanDiffMinMaxHeight(Grid g, Piece p, Point point)
{
    int min_height = scanMinHeight(g);
    int max_height = g.height;
    int diff_max_min1 = max_height - min_height;
    p.point = point;
    putInGrid(&g, p);
    int min_height2 = scanMinHeight(g);
    int max_height2 = g.height;
    int diff_max_min2 = max_height2 - min_height2;

    return (-25+(diff_max_min1 - diff_max_min2));
}
