#ifndef COMMON_H_INCLUDED
#define COMMON_H_INCLUDED

#define GRID char grille[12][25]
#define BLOC char bloc[4][4]

#define GAME_WIDTH 12
#define GAME_HEIGHT 25
#define SPEED 100
#define P_FULL '1'
#define P_EMPT '0'
#define G_EMPT '.'

//PIECES TYPES
#define P_O 0
#define P_L 1
#define P_T 2
#define P_Z 3
#define P_I 4
#define P_S 5


//POINTS PER LINE
#define ONE_FULL 40
#define TWO_FULL 100
#define THREE_FULL 300
#define FOUR_FULL 1200

//COLORS FOR TEXT
#define color(param) printf("\033[%sm",param)
#define C_DEFAULT "0"
#define C_RED "31"
#define C_GREEN "32"
#define C_YELLOW "33"
#define C_BLUE "34"
#define C_MAGENTA "35"
#define C_CYAN "36"

typedef struct Point Point;

struct Point
{
    int x;
    int y;
};

#endif // COMMON_H_INCLUDED
