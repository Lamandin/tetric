#ifndef AI_H_INCLUDED
#define AI_H_INCLUDED

#include "Grille.h"
#include "Common.h"
#include "Pieces.h"

Point guiguiAI(Grid *g, Piece *p);
Piece jujuAI(Grid g, Piece p);
int recetteMagique(int contactPiece, int contactGridY, int nbContactGridX, int nbHole, int diffHeight, int nbLineFulFilled);
#endif
