/*
 * Project: TetriC
 */
#include "AI.h"

Piece jujuAI(Grid g, Piece p)
{
    int x = 0;
    int rotation = 0;
    int nbContact = 0;
    int nbContactGridY = 0;
    int nbContactGridX = 0;
    int nbHole=0;
    int nbLineFulFilled=0;
    int score=0;
    int old_score=-10000;
    int diffHeight = 0;
    Point point_final; //Héhé
    int rotation_finale = 0;
    for(rotation=0; rotation<8; rotation++)
    {
        x=0;
        p.point.x = x;
        updatePiece(&p, rotation);
        while(canGoRight(g, p)==1)
        {
            Point p_pose;
            p.point.x=x;
            p_pose = getLowest(g,p);
            //printf("Lowest position found at x: %d, y: %d\n", p_pose.x, p_pose.y);
            nbContact = scanContactPointsWithPiece(g, p, p_pose);
            nbContactGridY = scanContactPointsWithGridY(g,p,p_pose);
            nbContactGridX = scanContactPointsWithGridX(g,p,p_pose);
            nbHole = scanHoleMade(g, p, p_pose);
            diffHeight = scanDiffHeight(g,p,p_pose);
            nbLineFulFilled = scanLineFulFilled(g, p, p_pose);
            score = recetteMagique(nbContact, nbContactGridY, nbContactGridX, nbHole, diffHeight, nbLineFulFilled);
            //printf("Scan Contact :%d \n", nbContact);
            if(score>old_score)
            {
                old_score=score;
                point_final=p_pose;
                rotation_finale = rotation;
                //printf("New Best Shot-> Score: %d position found at x: %d, y: %d and rotation is %d \n", score, point_final.x, point_final.y, rotation_finale);
            }
            x++;
        }
    }
    updatePiece(&p, rotation_finale);
    p.point = point_final;
    return p;

}

int recetteMagique(int contactPiece, int contactGridY, int nbContactGridX, int nbHole, int diffHeight, int nbLineFulFilled)
{
    //printf("contact piece: %d || contactGridY: %d || nbHole: %d || diffheight: %d\n",contactPiece, contactGridY, nbHole, diffHeight);
    return 3*contactPiece+(3*contactGridY)+nbContactGridX-(6*nbHole)-4*diffHeight+3*nbLineFulFilled;
}
