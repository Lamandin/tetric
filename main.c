/*
 * Project: TetriC
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "Pieces.h"
#include "Grille.h"
#include "Common.h"
#include "AI.h"
#include <unistd.h>

void debugJuju()
{
    int moyenne = 0;
    srand(time(NULL));
    int r = rand();
    printf("\n\n");
    int nbtetris = 10;
    int i=0;
    for(i=0; i<nbtetris; i++)
    {
        Grid g;
        Piece piece;
        initGrid(&g, G_EMPT);
        int height =0;
        while(height<24)
        {
            r = rand(); //New random for this row
            switch(r % 5)
            //Init of a piece using random
            {
                case 0:
                    initPiece(&piece, P_O, 0, 0); //Init it
                    break;
                case 1:
                    initPiece(&piece, P_L, 0, 0); //Init it
                    break;
                case 2:
                    initPiece(&piece, P_T, 0, 0); //Init it
                    break;
                case 3:
                    initPiece(&piece, P_Z, 0, 0); //Init it
                    break;
                case 4:
                    initPiece(&piece, P_I, 0, 0); //Init it
                    break;
                default:
                    printf("Error - Can't properly init the new piece. (1105301441)");
                    break;
            }
            //initPiece(&p, P_L, 0, 0);
            piece = jujuAI(g,piece);
            putInGrid(&g, piece);
            //afficheGrille(g);
            fullLines(&g);
            //getchar();
            removeFullLines(&g);
            height = heightGrid(g);
        }
        printf("Full lines: %d \n", g.totalLines);
        moyenne+=g.totalLines;

    }
    printf("Moyenne: %d \n", moyenne/nbtetris);
}

void debugGuigui()
{

    //Make it clean for the king :)
    system("clear");

    //Create a random
    srand(time(NULL));
    int r = rand();

    //Create & init data
    Piece piece; //Create your piece
    Grid g; //Create your grid
    int nbtour = 0;
    initGrid(&g, G_EMPT);//Full of space


    afficheGrille(g); //First draw
    while(g.height < 24)
    {
        r = rand(); //New random for each row

        switch(r % 5)
        //Init of a piece using random
        {
            case 0:
                initPiece(&piece, P_O, 0, 0); //Init it
                break;
            case 1:
                initPiece(&piece, P_L, 0, 0); //Init it
                break;
            case 2:
                initPiece(&piece, P_T, 0, 0); //Init it
                break;
            case 3:
                initPiece(&piece, P_Z, 0, 0); //Init it
                break;
            case 4:
                initPiece(&piece, P_I, 0, 0); //Init it
                break;
            default:
                printf("Error - Can't properly init the new piece. (1105301441)");
                break;
        }

        //Ask to our AI which place is the best fit for our piece
        piece = jujuAI(g,piece);
        putInGrid(&g, piece);
        system("clear");
        afficheGrille(g);
        fullLines(&g);
        printf("Full lines: %d \n", g.totalLines);
        usleep(8000);
        removeFullLines(&g);
        g.height = heightGrid(g);
    }
    printf("Full lines: %d \n", g.totalLines);
}

int main()
{
    int choice = 0;
    do
    {
        printf("Which debug function would you like to start?\n");
        printf("For Julien debug function type:     0\n");
        printf("For Guillaume debug function type:  1\n");
        printf("Choice: ");
        choice = getchar();
        system("clear");
    }while(choice != '0' && choice != '1' && choice != '2');

    if(choice == '1')
        debugGuigui();
    else
        debugJuju();
    return 0;
}