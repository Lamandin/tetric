#ifndef GRILLE_H_INCLUDED
#define GRILLE_H_INCLUDED
#include "Pieces.h"
#include "Common.h"
#include <stdio.h>

typedef struct Grid Grid;

struct Grid
{
    char grid[12][25];
    int height;
    int totalLines;
    int score;
};

void afficheGrille(Grid g);
void initGrid(Grid *g, char carac);
void afficheBloc(BLOC);
int canGoDown(Grid g, Piece p);
int canGoRight(Grid g, Piece p);
int canGoLeft(char grille[12][25], Piece p);
void recopieGrille(char grille[12][25], char copie[12][25]);
void putInGrid(Grid *g, Piece p);
int heightGrid(Grid g);
int fullLines(Grid *g);
void removeFullLines(Grid *g);
void shiftGrid(char grille[12][25], int y);
int LineToPoints(int numberofLines);

void dbgCreateLine(char grille[12][25], int y);

int scanContactPoints(Grid *g, Point point);
int scanContactPointsWithPiece(Grid g, Piece p, Point point);
int scanContactPointsWithGridY(Grid g, Piece p, Point point);
int scanContactPointsWithGridX(Grid g, Piece p, Point point);
int scanHoleMade(Grid g, Piece p, Point point);//That's not what you are thinking !
int scanDiffHeight(Grid g, Piece piece, Point p);
int scanLineFullFilled(Grid g, Piece piece, Point p);
int scanMinHeight(Grid g);
int scanDiffMinMaxHeight(Grid g, Piece p, Point point);
Point getLowest(Grid g, Piece p);
#endif // GRILLE_H_INCLUDED
